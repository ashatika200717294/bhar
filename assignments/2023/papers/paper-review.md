# Assignment

- Course Title: **Trends in Artificial Intelligence and Machine Learning**
- Course Code: **TAI911S**
- Assessment: Paper Review
- Released on: 03/05/2023
- Due Date: 01/06/2023

# Problem

## Problem Description

In this assignment, students are tasked to review a research paper assigned to them. Each review should include the following components:

1. summary of the paper, including
    
    * the problem being addressed;
    
    * the main contribution of the work;
    
    * the experimental/theoretical results;

2. The related work (previous research contributions related to the problem);

3. The limitations of the paper.

The list of papers is provided in the **References** section. The order of appearance of each paper represents the number attached to it and, therefore, should be attended to by the student assigned the same number in the allocation file. The latter is not disclosed to protect each student's information.

## Assessment Criteria

We will follow the criteria below to assess the problem:

- The quality and succinctness of the summary. (35%)
- A vibrant discussion of relevant (reasonable balance between the minimum and maximum numbers of papers) literature. (35%)
- The overall quality of the critique. (30%)

# Submission Instructions

- This assignment is to be completed individually.
- For each submission, a repository should be created on [Gitlab](https://about.gitlab.com).
- Within the repository, the submission should be in the form of a report in **PDF format**.
- The submission date is Thursday, June 01 2023, at midnight. Please note that _commits_ after that deadline will not be accepted. Therefore, a submission will be assessed based on the repository's clone at the deadline.
- Students who fail to submit will be awarded the mark 0.
- Students who fail to submit their assignment on time will have their marks deducted for every two days of delay.
- In the case of plagiarism (groups copying from each other or submissions copied from the Internet), all submissions involved will be awarded the mark 0, and each student will receive a warning.

## References

<a id="1">[1]</a>
Peters, Matthew and Neumann, Mark and Lyyer, Mohit and Gardner, Matt and Clark, Christopher and Lee, Kenton and Zettlemoyer, Luke.
Deep Contextualised Word Representations.
NAACL 2018, 2227--2237.

<a id="2">[2]</a>
Dai, Zihang and Yang, Zhilin and Yiming, Yang and Carbonell, Jaime G. and Viet, Quoc  Le and Salakhutdinov, Ruslan.
Transformer-XL: Attentive Language Models Beyond a Fixed-Length Content.
Conference of the Association for Computational Linguistics (ACL) 2019, 2978--2988.

<a id="3">[3]</a>
Howard, Jeremy  and Ruder, Sebastian.
Universal Language Model Fine-tuning for Text Classification.
Conference of the Association for Computational Linguistics (ACL) 2018, 328--339.

<a id="4">[4]</a>
Voita, Elena and Sennrich, Rico and Titov, Ivan.
Analyzing the Source and Target Contributions to Predictions in Neural Machine Translation.
Conference of the Association for Computational Linguistics (ACL) 2021, 1126--1140.

<a id="5">[5]</a>
Karras, Tero and Laine, Samuli and Aila, Timo.
A Style-Based Generator Architecture for Generative Adversarial Networks.

<a id="6">[6]</a>
Brock, Andrew and Donahue, Jeff and Simonyan, Karen.
Large Scale GAN Training for High Fidelity Natural Image Synthesis.
International Conference on Learning Representations (ICLR) 2019.

<a id="7">[7]</a>
Kingma, Diederik P. and  Salimans, Tim and Poole, Ben and Horock, Jonathan.
Variational Diffusion Models.

<a id="8">[8]</a>
Bieder, Florentin and Wolleb, Julia and Durrer, Alicia and Sandkuhler, Robin and Cattin Philippe C.
Diffusion Models for Memory-efficient Processing of 3D Medical Images.

<a id="9">[9]</a>
Dhinagar, Nikhil J. and Thomopoulos, Sophia I. and Laltoo, Emily and Thompson, Paul M.
Efficiently Training Vision Transformers on Structural MRI Scans for Alzheimer’s Disease Detection.

<a id="10">[10]</a>
Liu, Haohe and Chen, Zehua and Yuan, Yi and Mei, Xinhao and Liu, Xubo and Mandic, Danilo and Wang, Wenwu and Plumbley, Mark D.
AudioLDM: Text-to-Audio Generation with Latent Diffusion Models.

<a id="11">[11]</a>
Qian, Lihua and Wang, Mingxuan and Liu, Yang and Zhou, Hao
Diff-Glat: Diffusion Glancing Transformer for Parallel Sequence to Sequence Learning.

<a id="12">[12]</a>
He, Zhengfu and Sun, Tianxiang and Wang, Kuanning and Huang, Xuanjing and Qiu, Xipeng.
DiffusionBERT: Improving Generative Masked Language Models with Diffusion Models.

<a id="13">[13]</a>
Dieleman, Sander and Sartran, Laurent and Roshannai, Arman and Savinov, Nikolay and Ganin, Yaroslav and Richemond, Pierre H. and Doucet, Arnaud and  Strudel, Robin and  Dyer, Chris and Durkan, Conor and Hawthorne, Curtis andLeblond, Rémi and Grathwohl, Will and Adler, Jonas.
Continuous diffusion for categorical data.

<a id="14">[14]</a>
Chang, Ping and Li, Huayu and Quan, Stuart F. and Lu, Shuyang and Wung, Shu-Fen and Roveda, Janet and Li, Ao.
TDSTF: Transformer-based Diffusion probabilistic model for Sparse Time series Forecasting

<a id="15">[15]</a>
Tashiro, Yusuke and Song, Jiaming and Song, Yang and Ermon, Stefano.
CSDI: Conditional Score-based Diffusion Models for Probabilistic Time Series Imputation.

<a id="16">[16]</a>
Regol, Florence and Coates, Mark.
Diffusing Gaussian Mixtures for Generating Categorical Data.

<a id="17">[17]</a>
Wang, Wenjie and Xu, Yiyan and Feng, Fuli and Lin, Xinyu and He, Xiangnan and Chua, Tat-Seng.
Diffusion Recommender Model.

<a id="18">[18]</a>
Jang, Hyosoon and Mo, Sangwoo and Ahn, Sungsoo.
Diffusion Probabilistic Models for Graph-Structured Prediction.

<a id="19">[19]</a>
Welling, Max and Teh, Yee Whye. 
Bayesian Learning via Stochastic Gradient Langevin Dynamics.

<a id="20">[20]</a>
Gong, Wenbo and Li, Yingzhen.
Interpreting diffusion score matching using normalizing flow.

<a id="21">[21]</a>
Grinsztajn, Léo and Oyallon, Edouard and Varoquaux, Gaël.
Why do tree-based models still outperform deep learning on tabular data?

<a id="22">[22]</a>
Pedretti, Giacomo and Graves, Catherine E. and Serebryakov, Sergey and Mao, Ruibin and Sheng, Xia and Foltin, Martin and Li, Can and Strachan, JohnPaul.
Tree-based machine learning performed in-memory with memristive analog CAM.

<a id="23">[23]</a>
Asadi, Nima and Lin, Jimmy and de Vries, Arjen P.
Runtime Optimizations for Tree-Based Machine Learning Models.

<a id="24">[24]</a>
Yang, Yongxin and Garcia Morillo, Irene and Hospedales, Timothy M.
Deep Neural Decision Trees.

<a id="25">[25]</a>
Chen, Hongge and Zhang, Huan and Si, Si and Li, Yang and Boning, Duane and Hsieh, Cho-Jui.
Robustness Verification of Tree-based Models.

<a id="26">[26]</a>
Ghorbani, Amirata and Berenbaum, Dina and Ivgi, Maor and Dafna, Yuval and Zou, James.
Beyond Importance Scores: Interpreting Tabular ML by Visualizing Feature Semantics.

<a id="27">[27]</a>
Brini, Alessio and Giovannini, Elisa and Smaniotto, Elia.
A Machine Learning Approach to Forecasting Honey Production with Tree-Based Methods.

<a id="28">[28]</a>
Arabameri, Alireza and Pal, Subodh Chandra and Rezaie, Fatemeh and Chakrabortty, Rabin and Saha, Asish and Blaschke, Thomas.
Decision tree based ensemble machine learning approaches for landslide susceptibility mapping.

<a id="29">[29]</a>
Patalas-Maliszewska, Justyna and Łosyk, Hanna and Rehm, Matthias.
Decision-Tree Based Methodology Aid in Assessing the Sustainable Development of a Manufacturing Company.

<a id="30">[30]</a>
Zhang, Tingyu and Quevedo, Renata Pacheco and Wang, Huanyuan and Fu, Quan and Luo, Dan and Wang, Tao and Garcia de Oliveira, Guilherme and Antonio, Laurindo and Daleles Renno, Guasselli and Daleles Renno, Camilo.
Improved tree-based machine learning algorithms combining with bagging strategy for landslide susceptibility modeling.

<a id="31">[31]</a>
Kong, Shuming  and Shen, Yanyan and Huang, Linpeng.
Resolving Training Biases via Influence-Based Relabeling.

<a id="32">[32]</a>
Giunchiglia, Eleonora and Stoian, Mihaela Catalina and Lukasiewicz, Thomas.
Deep Learning with Logical Constraints.

<a id="33">[33]</a>
Chakrabarti, Deepayan and Fauber, Benjamin.
Robust High-Dimensional Classification From Few Positive Examples.

<a id="34">[34]</a>
Liu, Zhenguang and Wu, Sifan and Xu, Chejian and Wang, Xiang and Zhu, Lei and Wu, Shuang Feng, Fuli.
Copy Motion From One to Another: Fake Motion Video Generation.

<a id="35">[35]</a>
Wang, Hu and Ye, Mao and Zhu, Xiatian and Li, Shuai and Zhu, Ce and Li, Xue.
KUNet: Imaging Knowledge-Inspired Single HDR Image Reconstruction.

<a id="36">[36]</a>
Chen, Huili and Ding, Jie and Tramel, Eric William and Wu, Shuang and Sahu, Anit Kumar and Avestimehr, Salman and Zhang, Tao.
Self-Aware Personalized Federated Learning.

<a id="37">[37]</a>
Hu, Jie and Doshi, Vishwaraj and Eun, Do Young.
Efficiency Ordering of Stochastic Gradient Descent.

<a id="38">[38]</a>
Yun, Chulhee and Rajput, Shashank and Sra, Suvrit.
Minibatch vs Local SGD with Shuffling: Tight Convergence Bounds and Beyond.

<a id="39">[39]</a>
Guo, Minghao and Thost, Veronika and Li, Beichen and Das, Payel and Chen, Jie and Matusik, Wojciech.
Data-Efficient Graph Grammar Learning for Molecular Generation.

<a id="40">[40]</a>
Chen, Shuxiao and Crammer, Koby and He, Hangfeng and Roth, Dan and Su, Weijie J.
Weighted Training for Cross-Task Learning.

<a id="41">[41]</a>
Bao, Hangbo and Dong, Li and Piao, Songhao and Wei, Furu.
BEiT: BERT Pre-Training of Image Transformers.
