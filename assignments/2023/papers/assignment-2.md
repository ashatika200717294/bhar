# Assignment

- Course Title: **Trends in Artificial Intelligence and Machine Learning**
- Course Code: **TAI911S**
- Assessment: Second Assignment
- Released on: 11/04/2023
- Due Date: 02/05/2023

# Problem

## Problem Description

Consider the dataset available at this [link](https://www.kaggle.com/datasets/harrywang/wine-dataset-for-clustering).
It contains various attributes of wine, including alcohol, magnesium, hue, etc. Your task is twofold:

- Implement an **agglomerative hierarchical clustering** algorithm to determine the types of wine;
- Building on the classes identified earlier, implement a classifier using a **boosting** ensemble technique. 

Note that all your models must be implemented in the **Julia** programming language. 

## Assessment Criteria

We will follow the criteria below to assess the problem:

- Data preparation and preprocessing. (10%)
- Implementation of the clustering algorithm. (25%)
- Implementation of the classifier. (30%)
- Evaluation Metrics (for the classifier). (15%)
- Overall solution in **Julia**. (20%)

# Submission Instructions

- This assignment is to be completed individually.
- For each group, a repository should be created on [Gitlab](https://about.gitlab.com).
- The submission date is Tuesday, May 2 2023, at midnight. Please note that _commits_ after that deadline will not be accepted. Therefore, a submission will be assessed based on the repository's clone at the deadline.
- Students who fail to submit on time will be awarded the mark 0.
- Each student is expected to present his/her project after the submission deadline.
- In the case of plagiarism (groups copying from each other or submissions copied from the Internet), all submissions involved will be awarded the mark 0, and each student will receive a warning.
